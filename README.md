# Author2Day FB2 book exporter for Telegram

Service which is allow you to get fb2 version generated from HTML version of books listed in your library on the [Author2day](https://author.today/)  portal.

**#PetPoject of [kovalenkokv@gmail.com](mailto:kovalenkokv@gmail.com)**

I have no time to read books but like it, and I have found for myselfs alternative by using ["@voice aloud reader"](http://www.seeingwithsound.com/android.htm) adroid app. This app is alouding the text, htlm, fb2 book. Most of the books I've bought, have only HTML (read online) version on the autor2day site. It is not comfortable for me, because is required me to download each chapter and open it in the @voice app.

- This service allow to get fb2 version of book genereted from HTML verion.
- keep in touch with releases of favorite books.

Thank you wery much:
- [Alexandr Makurin](https://github.com/Ae-Mc/AuthorTodayToFB2Converter) - for his idea, and your project I've forked :)
- [Peter Meijer](https://play.google.com/store/apps/developer?id=Peter+Meijer) for your great app, this app allow me listning book than i'm driving a car or working.
- [Author2day](https://author.today/) - it is grate universe of dreams what you share with people.

# How to Run
1) git clone https://gitlab.com/kovalenkokv/author2day_exporter.git
2) cd author2day_exporter
3) create telegram bot [token](https://core.telegram.org/bots#6-botfather)
4) docker build -t a2de .
5) docker run --rm --name a2d --detach -e email="your author.today account email" -e pass="author.today pass" -e TOKEN='telegram bot token' -e private_id='[Your telegram ID](https://bigone.zendesk.com/hc/en-us/articles/360008014894-How-to-get-the-Telegram-user-ID-)'  a2de
