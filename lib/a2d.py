import requests
import config
from bs4 import BeautifulSoup

class a2d_client:
    session = None
    profile= None
    def __init__(self):
        self.session = requests.session()
        self.session.headers
        self.session.headers["User-Agent"] = (
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) "
            "AppleWebKit/537.36 (KHTML, like Gecko) "
            "Chrome/85.0.4183.83 Safari/537.36")
        self.session.headers["dnt"] = '1'
        self.session.headers["cache"] = "no-cache"
        self.session.headers["pragma"] = "no-cache"
        self.session.headers["Upgrade-Insecure-Requests"] = "1"
        self.session.headers["Sec-Fetch-Dest"] = "document"
        self.session.headers["Sec-Fetch-Mode"] = "navigate"
        self.session.headers["Sec-Fetch-Site"] = "same-origin"
        self.session.headers["Sec-Fetch-User"] = "?0"
        pass

    def Authorize(self, email: str, password: str) -> bool:
        try:
            loginPage = self.session.get(config.main+config.login)
            loginPage.raise_for_status()
        except:
            return False
        DOM: BeautifulSoup = BeautifulSoup(loginPage.text, "html.parser")
        tokenInput = DOM.select_one("form#logoffForm > input")
        requestVerificationToken = tokenInput.attrs["value"] if tokenInput else ''

        self.session.headers["__RequestVerificationToken"] = requestVerificationToken
        data = {
            "__RequestVerificationToken": requestVerificationToken,
            "Login": email,
            "Password": password
        }
        try:
            loginResponse = self.session.post(config.main+config.login, data=data)
            loginResponse.raise_for_status()
        except:
            return False
        del self.session.headers["__RequestVerificationToken"]
        self.GetUser()
        return True



    def GetUser(self):
        personalAccountResponse = self.session.get(config.main+config.personalAccount)
        personalAccountResponse.raise_for_status()
        soup = BeautifulSoup(personalAccountResponse.text, "html.parser")
        self.profile = soup.find_all("a", {"class": "link-with-icon"}, href=True)[8].get('href')


    def Logoff(self) -> bool:
        try:
            mainPage = self.session.get(config.main)
            mainPage.raise_for_status()
        except:
            return False

        DOM: BeautifulSoup = BeautifulSoup(mainPage.text, "html.parser")
        tokenInput = DOM.select_one("form#logoffForm > input")
        requestVerificationToken = tokenInput.attrs["value"] if tokenInput else ''
        self.session.headers["__RequestVerificationToken"] = requestVerificationToken
        data = {
            "__RequestVerificationToken": requestVerificationToken
        }
        try:
            logoffResponse = self.session.post(config.main+config.logoff, data=data, allow_redirects=False)
        except:
            return False
        if logoffResponse.status_code != 302:
            print("Error! Can't log off!"
                  f" Error code: {logoffResponse.status_code}")
            return False
        return True
    def get_reading_books(self,url):
        if self.profile == None:
            return None
        resp = self.session.get(config.main+self.profile+url)
        resp.raise_for_status()
        soup = BeautifulSoup(resp.text, "html.parser")
        books = {}
        for book in soup.find_all("div", {"class": "bookcard"}):
            status = None
            try:
                status = str(book.findNext("div", {"class":"book-status"}).span.text).replace('\xa0', '')
            except:
                status = None
                pass
            books[book['data-work-id']] = {
                'title': book.findNext("div", {"class":"bookcard-footer"}).h4.a.text,
                'img': book.findNext("div", {"class":"cover-image fade-box ebook-cover-image"}).img['data-src'],
                'status': status
            }
        return books
    #TODO_done list reading library status


