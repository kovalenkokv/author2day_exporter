#TODO_done Create fb2 file from the html pages
#https://pypi.org/project/FB2/

from datetime import datetime
from typing import List, Optional,cast, Union
from bs4 import BeautifulSoup, Tag
import re
from FB2 import FictionBook2
from FB2.Author import Author
import os
from datetime import date
from lib.a2d import a2d_client
import config
from .Dataclasses import ChapterHeader, User,Pages,Sequence

class fb2_file:
    def __init__(self, a2d:a2d_client):
        self.fb2 = FictionBook2()
        self.a2d:a2d_client = a2d

    def make_fb2(self,book_id):
        book: Book = Book(self.a2d)
        book.GetBookFromUrl("https://author.today/work/"+str(book_id))
        self.fb2.titleInfo.title = book.header.title
        self.fb2.titleInfo.authors = cast(
                                        List[Union[str , Author]] ,
                                        book.header.authors ,)
        self.fb2.titleInfo.annotation = book.header.annotation
        self.fb2.titleInfo.genres = book.header.genres
        self.fb2.titleInfo.lang = "ru"
        self.fb2.titleInfo.sequences = (
            [(book.header.sequence.name , book.header.sequence.number)]
            if book.header.sequence
            else None
        )
        self.fb2.titleInfo.keywords = book.header.tags
        self.fb2.titleInfo.coverPageImages = (
            [book.header.coverImageData]
            if book.header.coverImageData
            else None
        )
        self.fb2.titleInfo.date = (book.header.publicationDate , None)

        self.fb2.chapters = list(
            map(
                lambda chapter : (chapter.header.title , chapter.paragraphs) ,
                book.chapters ,
            )
        )
        pass

    def save(self):
        dir_path = config.save_folder+str(date.today().year)+'/'+str(date.today().month)+'/'+str(date.today().day)
        try:
            os.makedirs(dir_path, exist_ok=True)
        except:
            return None, f"Can't create folder: {dir_path}"
        path = dir_path+"/"+self.fb2.titleInfo.title+'.fb2'
        try:
            self.fb2.write(path)
        except:
            return None, f"Can't create file:{path}"
        return path, f"File created: {self.fb2.titleInfo.title}.fb2"



class BookHeader:
    title: str
    annotation: str
    bookId: int
    sequence: Optional[Sequence]
    genres: List[str]
    authors: List[str]
    tags: List[str]
    tableOfContents: List[ChapterHeader]
    coverImageData: Optional[bytes]
    publicationDate: datetime

    def __init__(self):
        self.authors = []

    def GetBookHeaderFromUrl(
        self, url: str, client: a2d_client.session
    ) -> None:
        bookPageResponse = client.get(url)
        bookPageResponse.raise_for_status()
        DOM: BeautifulSoup = BeautifulSoup(
            bookPageResponse.text, "html.parser"
        )
        bookPanel = DOM.select_one(".book-panel > .panel-body")
        if bookPanel:
            self._GetBookHeaderFromBookPanel(bookPanel, client)

    def GetReaderUrl(self) -> str:
        return Pages.baseReaderUrl + "/" + str(self.bookId)

    def GetChapterReaderUrl(self, chapter: ChapterHeader) -> str:
        return Pages.baseReaderUrl + f"/{self.bookId}?chapterId={chapter.id}"

    def GetChapterDataUrl(self, chapter: ChapterHeader) -> str:
        return Pages.baseReaderUrl + f"/{self.bookId}/chapter?id={chapter.id}"

    def _GetBookHeaderFromBookPanel(
        self, bookPanel: Tag, client: a2d_client.session
    ) -> None:
        bookTitle = bookPanel.select_one(".book-title")
        self.title = bookTitle.text.strip() if bookTitle else ""
        for author in bookPanel.select(".book-authors > span > meta"):
            self.authors.append(author.attrs["content"])
        bookAnnotation = bookPanel.select_one(".annotation > .rich-content")
        self.annotation = bookAnnotation.text.strip() if bookAnnotation else ""
        bookReaderLink = bookPanel.select_one(
            ".book-cover > a.book-cover-content"
        )
        self.bookId = (
            int(str(config.main + bookReaderLink.attrs["href"]).split("/")[-1]) if bookReaderLink  else -1)
        self.tableOfContents = self._GetBookTableOfContentsFromBookPanel(bookPanel)
        self.coverImageData = self._GetBookCoverFromBookPanel(bookPanel, client)
        self.sequence = self._GetBookSequenceFromBookPanel(bookPanel)
        self.genres = [a.text for a in bookPanel.select(".book-genres > a")]
        self.tags = [a.text for a in bookPanel.select(".tags > a")]
        bookPublicationDate = bookPanel.find(
            "span",
            attrs={"class": "hint-top", "data-format": "calendar-short"},
        )
        self.publicationDate = (
            datetime.fromisoformat(
                bookPublicationDate.attrs["data-time"][:-2].replace("T", " ")
            )
            if bookPublicationDate
            else datetime.fromtimestamp(0)
        )

    def _GetBookTableOfContentsFromBookPanel(
        self, bookPanel: Tag
    ) -> List[ChapterHeader]:
        tableOfContents = []
        toc = bookPanel.select_one("ul.table-of-content")
        if toc:
            if len(toc.select("li > a")) < len(toc.select("li")):
                print(
                    f"WARNING! Book «{self.title}» has blocked chapters!"
                    " Maybe you didn't authorize or didn't purchase it?"
                )
            for row in toc.select("li > a"):
                tableOfContents.append(ChapterHeader(row.text, int(row.attrs["href"].split("/")[-1])))
        return tableOfContents

    def _GetBookCoverFromBookPanel(
        self, bookPanel: Tag, client: a2d_client.session
    ) -> Optional[bytes]:
        coverImageTag = bookPanel.select_one(
            ".book-action-panel .book-cover .cover-image"
        )
        if coverImageTag is None:
            return None
        else:
            imageUrl = coverImageTag.attrs["src"].split("?")[0]
            coverImageResponse = client.get(imageUrl)
            coverImageResponse.raise_for_status()
            coverImageData = bytes(coverImageResponse.content)
            return coverImageData

    def SearchGroupOne(self,pattern: str , text: str) -> str :
        expr = re.compile(pattern)
        searchResult = expr.search(text)
        if searchResult is not None :
            return searchResult.group(1)
        else :
            return ""

    def _GetBookSequenceFromBookPanel(
        self, bookPanel: Tag
    ) -> Optional[Sequence]:
        seqSpan = bookPanel.find(
            "span", attrs={"class": "text-muted"}, text="Цикл: "
        )
        if seqSpan is None:
            return None

        seqName = seqSpan.findNext("a")
        seqNumber = seqName.findNext("span")
        sequence = Sequence(seqName.text, int(self.SearchGroupOne(r"(\d+)", seqNumber.text)))
        if len(sequence.name) == 0 or sequence.number == 0:
            return None
        return sequence

    def __str__(self):
        return (
            "{}. «{}» (".format(", ".join(self.authors), self.title)
            + (
                "sequence: {} #{}, ".format(
                    self.sequence.name, self.sequence.number
                )
                if self.sequence is not None
                else ""
            )
            + "id: {}, publication date: {})"
            "\nGenres: {}\nTags: {}\nAnnotation: {}"
        ).format(
            self.bookId,
            self.publicationDate,
            ", ".join(self.genres),
            ", ".join(self.tags),
            self.annotation,
        )

    def __repr__(self):
        return (
            "{}. «{}» (".format(", ".join(self.authors), self.title)
            + "sequence: {} #{}, ".format(
                self.sequence.name, self.sequence.number
            )
            if self.sequence is not None
            else "" + "id: {}, publication date: {})"
            "\nGenres: {}\nTags: {}\nAnnotation: {}"
        ).format(
            self.bookId,
            self.publicationDate,
            ", ".join(self.genres),
            ", ".join(self.tags),
            self.annotation,
        )



class Chapter:
    header: Optional[ChapterHeader]
    paragraphs: List[str]
    userId: int
    client: a2d_client.session

    def __init__(self,
                 header: ChapterHeader,
                 client: a2d_client.session = None,
                 user: User = None):
        self.client = client if client else a2d_client().session
        self.userId = user.userId if user else -1
        self.header = header

    def GetChapterFromUrl(self, url: str):
        chapterResponse = self.client.get(config.main+url)
        chapterResponse.raise_for_status()
        data = chapterResponse.json()
        if data["isSuccessful"] is True:
            text = data["data"]["text"]
            readerSecret = chapterResponse.headers["Reader-Secret"]
            chapterText = self._DecodeChapter(text, readerSecret)
            DOM: BeautifulSoup = BeautifulSoup(chapterText, "html.parser")
            self.paragraphs = []
            for paragraph in DOM.find_all("p"):
                self.paragraphs.append(paragraph.text)
        else:
            print(f"Error! Can't get chapter from url «{url}».")
            if "messages" in data and len(data["messages"]) > 0:
                if data["messages"][0] == "Paid":
                    print("Error! Book not purchased!")
                elif data["messages"][0] == "Unauthorized":
                    print("Error! Unauthorized!")
                else:
                    print(f"Error messages: ", end="")
                    print(*data['messages'],
                          sep=", ",
                          end="\n" if data['messages'][-1].endswith('.')
                          else ".\n")

    @staticmethod
    def GetUserId(client: a2d_client.session) -> int:
        personalAccountResponse =  client.get(Pages.main+Pages.personalAccount)
        personalAccountResponse.raise_for_status()
        responseText = personalAccountResponse.text
        searchResult = re.search(r"ga\('set', 'userId', '(\d*)'\)",
                                 responseText)
        if searchResult is not None:
            userId = int(searchResult.group(1))
        else:
            userId = -1
        return userId

    def _DecodeChapter(self,
                       chapterRawTextData: str,
                       readerSecret: str) -> str:
        cipher = "".join(reversed(readerSecret)) + "@_@"
        if self.userId != -1:
            cipher += str(self.userId)
        # encode and remove header
        l = list(chapterRawTextData.encode('utf-16'))[2:]
        # concatenate two adjacent bytes
        chapterEBytes = [(b<<8)|a for a,b in zip(l[0::2],l[1::2])]
        # utf-16 header
        chapterDBytes = [65279]
        for i in range(0, len(chapterEBytes)):
            chapterDBytes.append(chapterEBytes[i] ^ ord(cipher[i % len(cipher)]))
        # split by bytes, concatenate sequences and decode
        return bytes([b for a in chapterDBytes for b in [a>>8, a&0xff]]).decode('utf-16')

    def __str__(self):
        return (("" if self.header is None else self.header.title + "\n")
                + "\t" + "\n\t".join(self.paragraphs))

class Book:
    header: BookHeader
    client: a2d_client.session
    chapters: List[Chapter]

    def __init__(self, client:a2d_client):
        if client is None:
            a2d = a2d_client()
            a2d.Authorize(os.getenv("email") , os.getenv("pass"))
            self.client = a2d.session
        else:
            self.client = client.session
        self.header = BookHeader()

    def GetBookFromUrl(self, url: str):
        self.header.GetBookHeaderFromUrl(url, self.client)
        self.GetBookChapters()

    def GetBookChapters(self) -> List[Chapter]:
        self.chapters = []
        user = User("", "", Chapter.GetUserId(self.client))
        if len(self.header.tableOfContents) > 1:
            self.getMultipleChapters(user)
        else:
            self.getSingleChapter(user)
        return self.chapters

    def getMultipleChapters(self, user: User):
        tasks: List = []
        for chapterHeader in self.header.tableOfContents:
            tasks.append(
                    self.GetBookChapter(
                        self.header.GetChapterDataUrl(chapterHeader),
                        chapterHeader,
                        user,
                    )
            )
        for task in tasks:
            self.chapters.append(task)

    def SearchGroupOne(pattern: str , text: str) -> str :
        expr = re.compile(pattern)
        searchResult = expr.search(text)
        if searchResult is not None :
            return searchResult.group(1)
        else :
            return ""

    def getSingleChapter(self, user: User):
        readerPage =  self.client.get(config.main+self.header.GetReaderUrl())
        readerPage.raise_for_status()
        chapterId = int(self.SearchGroupOne(r"chapterId: (\d+),", readerPage.text))
        chapterHeader = ChapterHeader(self.header.title, chapterId)
        self.chapters.append(
            self.GetBookChapter(
                self.header.GetChapterDataUrl(chapterHeader),
                chapterHeader,
                user,
            )
        )

    def GetBookChapter(
        self, url: str, chapterHeader: ChapterHeader, user: User
    ) -> Chapter:
        chapter = Chapter(chapterHeader, self.client, user)
        chapter.GetChapterFromUrl(url)
        return chapter


