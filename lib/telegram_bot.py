#TODO_done telegram bot (https://github.com/eternnoir/pyTelegramBotAPI)
    #TODO_done Provide fb2 of the book.
    #TODO restart session a2d
    #TODO_done Autorization by the telegramID
    #TODO_done provide info list of books with information if any changes

import telebot
from telebot import types
import os
from lib.a2d import a2d_client
import logging
from lib.fb2_creator import fb2_file

class tbot:

    def __init__(self, log:logging):
        self.bot = telebot.TeleBot(os.getenv("TOKEN"), parse_mode=None)
        self.auth_user = int(os.getenv('private_id'))
        self.log:logging = log



        @self.bot.message_handler(commands=['start', 'help','menu'])
        def start_message(message):
            log.info(str(message.from_user.id)+" /start")
            if message.from_user.id == self.auth_user:
                markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
                btn1 = types.KeyboardButton("ViewRB")
                btn2 = types.KeyboardButton("ViewRB Updated")
                btn3 = types.KeyboardButton("ViewRB Later")
                btn4 = types.KeyboardButton("ViewRB Later Updated")
                markup.add(btn1,btn2,btn3,btn4)
                self.bot.send_message(message.chat.id , "Menu:" , reply_markup=markup)

            else:
                self.bot.reply_to(message , "Unknown user")


        @self.bot.message_handler(content_types='text')
        def message_reply(message) :
            if message.from_user.id == self.auth_user :
                if message.text == "ViewRB" :
                    self.commands =[]
                    list = self.a2d.get_reading_books("/library/reading")
                    for book in list:
                        self.bot.send_photo(message.chat.id ,photo=list[book]['img'],caption=f"/fb{book}\n Title: {list[book]['title']}\n Status: {str(list[book]['status']).replace('None','--')}\n https://author.today/work/{book}")
                        self.commands.append(book)
                elif "ViewRB Updated" == message.text:
                    self.commands = []
                    list = self.a2d.get_reading_books("/library/reading")
                    for book in list:
                        if list[book]['status'] !=None:
                            self.bot.send_photo(message.chat.id ,photo=list[book]['img'],caption=f"/fb{book}\n Title: {list[book]['title']}\n Status: {str(list[book]['status']).replace('None','--')}\n https://author.today/work/{book}")
                            self.commands.append(book)
                elif "ViewRB Later" == message.text:
                    self.commands = []
                    list = self.a2d.get_reading_books("/library/saved")
                    for book in list:
                        self.bot.send_photo(message.chat.id ,photo=list[book]['img'],caption=f"/fb{book}\n Title: {list[book]['title']}\n Status: {str(list[book]['status']).replace('None','--')}\n https://author.today/work/{book}")
                        self.commands.append(book)
                elif "ViewRB Later Updated" == message.text:
                    self.commands = []
                    list = self.a2d.get_reading_books("/library/saved")
                    for book in list:
                        if list[book]['status'] !=None:
                            self.bot.send_photo(message.chat.id ,photo=list[book]['img'],caption=f"/fb{book}\n Title: {list[book]['title']}\n Status: {str(list[book]['status']).replace('None','--')}\n https://author.today/work/{book}")
                            self.commands.append(book)
                elif message.text[0:3] == "/fb":
                    fb = fb2_file(self.a2d)
                    fb.make_fb2(message.text[3:])
                    filepath, tmp = fb.save()
                    if filepath !=None:
                        doc = open(filepath,'rb')
                        self.bot.send_document(message.chat.id, doc,caption=tmp)
                        doc.close()
                    else:
                        self.bot.send_message(message.chat.id,f"Fb2 creation error: {tmp}")

            else:
                self.bot.reply_to(message , "Unknown user")


    def pulling(self):
        self.a2d = a2d_client()
        self.a2d.Authorize(os.getenv("email") , os.getenv("pass"))
        self.bot.infinity_polling()





