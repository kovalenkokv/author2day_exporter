FROM python:3.9-slim

COPY requirements.txt .
ARG email=local
ARG pass=local
ARG TOKEN=local
ARG private_id=local
ENV email ${email}
ENV pass ${pass}
ENV TOKEN ${TOKEN}
ENV private_id ${private_id}


RUN pip3 install -r requirements.txt

RUN mkdir /script
RUN mkdir /script/lib
RUN mkdir /script/Logs
WORKDIR /script

COPY ./*.py /script/
COPY ./lib/*.py /script/lib/

CMD ["python3", "/script/main.py"]