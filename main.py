import logging
from lib.telegram_bot import tbot
from datetime import date
if __name__ == "__main__":

    logging.basicConfig(filename='Logs/' + date.today().strftime("%Y-%m-%d") + 'a2d_exporter.log' ,
                        level=logging.INFO , format='%(asctime)s %(message)s')
    t = tbot(logging).pulling()
