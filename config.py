main = "https://author.today"
login = "/account/login"
logoff = "/account/logoff"
personalAccount = "/account/my-page"
profile = "/u/{USERNAME}"
purchased = "/u/{USERNAME}/library/purchased"
baseReaderUrl = "/reader"
save_folder = './fb2/'